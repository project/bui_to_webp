CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The main idea behind this module is to optimize the page performance by
doing image compression.
Using this module you can perform bulk update task on images. All the existing
images will be compressed based on the set image quality ( Configurable )
and will be converted to webp extension.


REQUIREMENTS
------------

This module requires the following modules:

 * ImageMagick (https://www.drupal.org/project/imagemagick)


RECOMMENDED MODULES
-------------------

No recommended modules.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

 * Visit the Bulk Update Images to Webp Settings form at
   (/admin/bui_to_webp/settings) or `Configuration >> Media >>
   Bulk Update Images to Webp Settings.`

 * Select from the list of entities and click `Run bulk update`.

 * Set the quality of compressed images and also check if lossless compression
   is required or not.

`NOTE: The webp extension will be added automatically in all the image type
fields.`


MAINTAINERS
-----------

Current maintainers:
 * Kunal Singh - https://www.drupal.org/u/kunal_singh
