<?php

namespace Drupal\bui_to_webp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\ConfigFormBaseTrait;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\imagemagick\ImagemagickExecManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * BulkUpdateImageAdminSettings to configure the module according to the need.
 *
 * @internal
 */
class BulkUpdateImagesAdminSettings extends FormBase {

  use ConfigFormBaseTrait;

  /**
   * Entity type manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityTypeRepository;

  /**
   * The ImageMagick execution manager service.
   *
   * @var \Drupal\imagemagick\ImagemagickExecManagerInterface
   */
  protected $execManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The constructor method.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Entity\EntityTypeRepositoryInterface $entity_type_repository
   *   The entity type repository.
   * @param \Drupal\imagemagick\ImagemagickExecManagerInterface $exec_manager
   *   The ImageMagick execution manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeRepositoryInterface $entity_type_repository, ImagemagickExecManagerInterface $exec_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeRepository = $entity_type_repository;
    $this->execManager = $exec_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the service required to construct this class.
      $container->get('entity_type.manager'),
      $container->get('entity_type.repository'),
      $container->get('imagemagick.exec_manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bui_to_webp.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bui_to_webp_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bui_to_webp.settings');
    // List of all the entites;.
    $all_entities = $this->entityTypeRepository->getEntityTypeLabels(TRUE)['Content'];

    $form['description'] = [
      '#markup' => '<p>' . $this->t('Bulk update will replace all the existing images with a new webp generated image.') . '</p>',
    ];
    $form['run_for_entities'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the entities for which you want to execute the bulk image update.'),
      '#options' => $all_entities,
    ];
    $state = [];
    foreach ($all_entities as $key => $value) {
      $state[] = [
        'input[name="run_for_entities[' . $key . ']"]' => ['checked' => TRUE],
      ];
    }
    $form['run'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run bulk update'),
      '#submit' => ['::runBulkUpdate'],
      '#states' => [
        'enabled' => $state,
      ],
    ];

    $form['disclaimer'] = [
      '#markup' => '<p><strong>' . $this->t('NOTE: The webp extension will be added automatically in all the image type fields.') . '</strong></p>',
    ];

    $form['bui_to_webp'] = [
      '#title' => $this->t('Bulk Update Images to Webp Settings'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $form['bui_to_webp']['quality'] = [
      '#type' => 'number',
      '#title' => $this->t('The compressed image quality.'),
      '#size' => 10,
      '#min' => 0,
      '#max' => 100,
      '#maxlength' => 3,
      '#required' => TRUE,
      '#default_value' => $config->get('quality'),
      '#field_suffix' => '%',
      '#description' => $this->t('Define the image quality of processed images. Ranges from 0 to 100. Higher values mean better image quality but bigger files.'),
    ];

    $form['bui_to_webp']['lossless'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable lossless compression'),
      '#description' => $this->t('Encode the image without any loss.'),
      '#default_value' => $config->get('lossless'),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('bui_to_webp.settings')
      ->set('quality', $form_state->getValue('quality'))
      ->set('lossless', $form_state->getValue('lossless'))
      ->save();
    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
  }

  /**
   * Bulk update all the image fields with webp image.
   */
  public function runBulkUpdate(array &$form, FormStateInterface $form_state) {
    // Execute only if imageMagick suite is set and path is defined.
    if ($this->config('system.image')->get('toolkit') == 'imagemagick') {
      $active_package = $this->execManager->getPackage();
      if ($active_package) {
        // Status of the binaries path.
        $binary_path = $this->config('imagemagick.settings')->get('path_to_binaries');
        if ($binary_path) {
          $status = $this->execManager->checkPath($binary_path);
          if (!empty($status['errors'])) {
            $this->messenger()->addError(new FormattableMarkup(implode('<br />', $status['errors']), []));
            return;
          }
        }
        else {
          $this->messenger()->addError($this->t('Please configure binary path in ImageMagick.'));
          return;
        }
      }
    }
    else {
      $this->messenger()->addError($this->t("Please select 'ImageMagick' as default toolkit."));
      return;
    }
    // Execute batch process.
    $entities = $form_state->getValue('run_for_entities');
    $entities = array_filter($entities, function ($item) {
      return $item != '0';
    });

    // Bulk update images.
    if ($entities) {
      $operations = [];
      // Get list of bundles having image type field.
      $entities_with_image = $this->entityFieldManager->getFieldMapByFieldType('image');
      foreach ($entities as $entity) {
        $operations[] = [
          '\Drupal\bui_to_webp\BulkUpdateManager::updateFields',
          [$entity, $entities_with_image],
        ];
      }
      $batch = [
        'title' => $this->t('Updating Fields...'),
        'progress_message' => $this->t('Completed @current out of @total chunks.'),
        'operations' => $operations,
        'finished' => '\Drupal\bui_to_webp\BulkUpdateManager::batchFinished',
        'file' => '\Drupal\bui_to_webp\BulkUpdateManager',
      ];
      batch_set($batch);
    }

  }

}
