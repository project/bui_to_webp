<?php

namespace Drupal\bui_to_webp;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\imagemagick\ImagemagickExecArguments;

/**
 * Bulk Image Update Manager class.
 */
class BulkUpdateManager {

  use StringTranslationTrait;

  /**
   * Update image field of individual entity.
   *
   * @param string $entity
   *   The machine name of entity.
   * @param array $entities_with_image
   *   The array of all the entites having image field.
   * @param array $context
   *   The batch context.
   */
  public static function updateFields(string $entity, array $entities_with_image, array &$context) {
    $entityTypeManager = \Drupal::entityTypeManager();
    $execManager = \Drupal::service('imagemagick.exec_manager');
    $arguments = new ImagemagickExecArguments($execManager);

    if (array_key_exists($entity, $entities_with_image)) {
      $field_image = $entities_with_image[$entity];
      $fields = [];
      foreach ($field_image as $key => $value) {
        if (array_key_exists('bundles', $value)) {
          $fields[$key] = $value['bundles'];
        }
      }
      // Update image field config to accept webp extension.
      self::updateImageFieldExtension(array_keys($fields));
      // Replace image with webp extension image.
      if (!empty($fields)) {
        foreach ($fields as $key => $bundles) {
          foreach ($bundles as $bundle) {
            $nids = $entityTypeManager->getStorage($entity)->getQuery()->condition($key, '', '<>')->accessCheck(TRUE)->execute();
            $nodes = $entityTypeManager->getStorage($entity)->loadMultiple($nids);
            // Loading node and fetching the image field.
            foreach ($nodes as $node) {
              $context['message'] = 'Processing node id ' . $node->id();
              $image = $node->get($key)->getValue()[0] ?? NULL;
              if (array_key_exists('target_id', $image)) {
                $file = $entityTypeManager->getStorage('file')->load($image['target_id']);
                // Check if the image is of webp type or not.
                if (!($file->getMimeType() == 'image/webp')) {
                  $uri = $file->getFileUri();
                  if (!empty($uri)) {
                    $fileSystem = \Drupal::service('file_system');
                    $real_path = $fileSystem->realPath($uri);
                    // Proceed only if image exists.
                    if (file_exists($real_path)) {
                      $uri_path_info = pathinfo($real_path);
                      $arguments->setSourceLocalPath($real_path);
                      // Generate destination image path.
                      $destination_img_path = $uri_path_info['dirname'] . '/' . $uri_path_info['filename'] . '.webp';
                      $arguments->setDestinationLocalPath($destination_img_path);
                      $config = \Drupal::config('bui_to_webp.settings');
                      $command = '-quality ' . $config->get('quality') . ' -define webp:lossless=' . $config->get('lossless');
                      $arguments->add($command);
                      $output = NULL;
                      $result = $execManager->execute('convert', $arguments, $output);
                      // Reset the arguments.
                      if ($result) {
                        $url_path = $file->createFileUrl();
                        $url_path_info = pathinfo($url_path);
                        $dirname = str_replace('/sites/default/files', 'public:/', $url_path_info['dirname']);
                        $new_url = $dirname . '/' . urldecode($url_path_info['filename']) . '.webp';
                        // Saving updated info.
                        $file->setFileUri($new_url);
                        $file->setMimeType('image/webp');
                        $file->setFileName($fileSystem->basename($new_url));
                        $file->setSize(filesize($new_url));
                        $file->save();

                        // Update the processed item.
                        $context['results'][] = $node->id();
                      }
                    }
                  }
                }
              }
              // Reset arguments.
              $arguments->reset();
            }
          }
        }
      }
    }
  }

  /**
   * Handle batch completion.
   *
   * @param bool $success
   *   TRUE if all batch API tasks were completed successfully.
   * @param array $results
   *   An array of resaved node IDs.
   * @param array $operations
   *   A list of the operations that had not been completed.
   */
  public static function batchFinished($success, array $results, array $operations) {
    if ($success) {
      \Drupal::messenger()->addMessage(t('Updated @count images.', [
        '@count' => count($results),
      ]));
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      ]);
      \Drupal::messenger()->addError($message);
    }
  }

  /**
   * Update image field with webp extension.
   *
   * @param array $fields
   *   Array of machine name of all the image fields.
   */
  public static function updateImageFieldExtension(array $fields) {
    // Return if array is empty.
    if (empty($fields)) {
      return;
    }
    // Processing all the fields and adding webp extension is not already
    // present.
    $entityTypeManager = \Drupal::entityTypeManager();
    foreach ($fields as $field) {
      $field_config = $entityTypeManager->getStorage('field_config')->loadByProperties(['field_name' => $field]);
      if ($field_config) {
        foreach ($field_config as $config) {
          $settings = $config->getSettings();
          if (array_key_exists('file_extensions', $settings)) {
            $extensions = explode(' ', $settings['file_extensions']);
            if (!in_array('webp', $extensions)) {
              array_push($extensions, 'webp');
            }
            $settings['file_extensions'] = implode(' ', $extensions);
          }
          $config->setSettings($settings);
          $config->save();
        }
      }
    }
  }

}
